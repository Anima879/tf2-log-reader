import unittest
from LogReader import *


class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.student = LogReader("test_log.log")

    def test_get_time(self):
        get_time_sample_test = (
            ('L 11/03/2019 - 16:45:34: "Güven<17><[U:1:52569854]><>" STEAM USERID validated"', '16:45:34'),
            ('L 11/03/2019 - 16:56:57: "Buddy Psycho<18><[U:1:8500674]><Red>" triggered "shot_fired" (weapon '
             '"degreaser")',
             '16:56:57'),
            ('L 11/03/2019 - 16:56:56: "trum.<19><[U:1:242658929]><Blue>" triggered "damage" against "ubii<6><['
             'U:1:138690922]><Red>" (damage "39") (weapon "tomislav")',
             '16:56:56'),
            ('L 11/03/2019 - 17:12:03: Team "Red" triggered "pointcaptured" (cp "0") (cpname "#koth_viaduct_cap") ('
             'numcappers "5") (player1 "ubii<6><[U:1:138690922]><Red>") (position1 "-1370 -79 229") (player2 '
             '"Reoccurring<9><[U:1:138146825]><Red>") (position2 "-1522 196 236") (player3 "Anima<15><['
             'U:1:168111589]><Red>") (position3 "-1431 44 230") (player4 "Buddy Psycho<18><[U:1:8500674]><Red>") ('
             'position4 "-1403 -43 228") (player5 "dabeste :\'><23><[U:1:67163975]><Red>") (position5 "-1547 -27 '
             '231")', "17:12:03")
        )
        for sample in get_time_sample_test:
            result = self.student.get_time(sample[0])
            self.assertEqual(sample[1], result)

    def test_get_date(self):
        get_date_sample_test = (
            ('L 11/03/2019 - 17:12:04: "ubii<6><[U:1:138690922]><Red>" triggered "shot_fired" (weapon "tomislav")',
             "11/03/2019"),
            ('L 11/03/2019 - 16:54:37: "Joehn<16><[U:1:67582501]><Blue>" triggered "shot_hit" (weapon '
             '"tf_projectile_pipe_remote")', "11/03/2019")
        )
        for sample in get_date_sample_test:
            result = self.student.get_date(sample[0])
            self.assertEqual(sample[1], result)

    def test_remove_date_time(self):
        remove_date_time_sample = (
            ('L 11/03/2019 - 16:45:34: "Güven<17><[U:1:52569854]><>" STEAM USERID validated"',
             '"Güven<17><[''U:1:52569854]><>" STEAM USERID validated"'),
            ('L 11/03/2019 - 16:56:57: "Buddy Psycho<18><[U:1:8500674]><Red>" triggered "shot_fired" (weapon '
             '"degreaser")',
             '"Buddy Psycho<18><[U:1:8500674]><Red>" triggered "shot_fired" (weapon '
             '"degreaser")'),
            ('L 11/03/2019 - 16:56:56: "trum.<19><[U:1:242658929]><Blue>" triggered "damage" against "ubii<6><['
             'U:1:138690922]><Red>" (damage "39") (weapon "tomislav")',
             '"trum.<19><[U:1:242658929]><Blue>" triggered "damage" against "ubii<6><['
             'U:1:138690922]><Red>" (damage "39") (weapon "tomislav")'),
            ('L 11/03/2019 - 17:12:03: Team "Red" triggered "pointcaptured" (cp "0") (cpname "#koth_viaduct_cap") ('
             'numcappers "5") (player1 "ubii<6><[U:1:138690922]><Red>") (position1 "-1370 -79 229") (player2 '
             '"Reoccurring<9><[U:1:138146825]><Red>") (position2 "-1522 196 236") (player3 "Anima<15><['
             'U:1:168111589]><Red>") (position3 "-1431 44 230") (player4 "Buddy Psycho<18><[U:1:8500674]><Red>") ('
             'position4 "-1403 -43 228") (player5 "dabeste : ><23><[U:1:67163975]><Red>") (position5 "-1547 -27 '
             '231")',
             'Team "Red" triggered "pointcaptured" (cp "0") (cpname "#koth_viaduct_cap") ('
             'numcappers "5") (player1 "ubii<6><[U:1:138690922]><Red>") (position1 "-1370 -79 229") (player2 '
             '"Reoccurring<9><[U:1:138146825]><Red>") (position2 "-1522 196 236") (player3 "Anima<15><['
             'U:1:168111589]><Red>") (position3 "-1431 44 230") (player4 "Buddy Psycho<18><['
             'U:1:8500674]><Red>") ('
             'position4 "-1403 -43 228") (player5 "dabeste : ><23><[U:1:67163975]><Red>") (position5 "-1547 '
             '-27 '
             '231")')
        )

        for sample in remove_date_time_sample:
            result = self.student.remove_date_time(sample[0])
            self.assertEqual(sample[1], result)

    def test_get_lines_by_username(self):
        with self.assertRaises(ValueError):
            self.student.get_lines_by_username("Anima", option=" ")

        # TODO : unitest.

    def test_get_team(self):
        team = self.student.get_player_team("Anima")
        self.assertEqual("Red", team)
        team = self.student.get_player_team("Joehn")
        self.assertEqual("Blue", team)

        with self.assertRaises(ValueError):
            self.student.get_player_team("player")
            self.student.get_player_team("Unassigned")
            self.student.get_player_team("Entered")

    def test_get_global_chat(self):
        pass
        # TODO : unitest


if __name__ == '__main__':
    unittest.main()
