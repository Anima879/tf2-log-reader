import re
from typing import List


class LogReader:
    """
        Class who read the logs and extract useful information.
    """

    events = (
        "shot_fired",
        "shot_hit",
        "damage",
        "empty_uber",
        "healed",
        "killedobject",
        "kill assist",
        "medic_death",
        "medic_death_ex",
        "first_heal_after_spawn",
        "domination",
        "revenge",
        "player_extinguished",
        "Game_Over",
        "Intermission_Win_Limit",
        "Round_Win",
        "Round_Length",
        "object_detonated",
        "pointcaptured"
    )

    connectors = (
        "against",
        "with"
    )

    actions = (
        "triggered",
        "say",
        "spawned as",
        "connected",
        "STEAM USERID validated",
        "entered the game",
        "changed role to",
        "say_team",
        "picked up item",
        "killed"
    )

    def __init__(self, log_path):
        """
            Read the whole logs.
        :param log_path: {string} relative or absolute path to the file.
        :var self.log: {list: string} contain each line of the log file.
        :var self.match_log: {list: string} Contain each line of the log concerning the match. (without the warm up)
        """

        with open(log_path, "r", encoding="utf8") as file:
            self.log = file.readlines()

        for i in range(len(self.log)):
            if 'World triggered "Round_Start"' in self.log[i]:
                break
        else:
            i = 0

        self.match_log = self.log[i:]

    def get_player_information(self, line, option="player"):
        """
            Gather information of the player according to what he is doing.
        :param line: {string} analyzed line.
        :param option: {string} three options : "player" : the player did the
        action. "target" : the player received the action. "all" : both of them.
        :raise {ValueError} if option does not match any case.
        :return: {dic} dic with information.
        """
        # TODO : get player's information.
        if option == "all":
            pass
        elif option == "player":
            pass
        elif option == "target":
            pass
        else:
            raise ValueError("Invalid option")

    def get_lines_by_username(self, name, option="all"):
        """
            Give the lines according to the name and the options.
        :param name: {string} player's name
        :param option: {string} three options : "player" : lines where the player did the
        action. "target" : lines where the player received the action. "all" : lines where
        the player did and received the action.
        :raise {ValueError} if option does not match any case.
        :return: {list: string} list of the lines.
        """
        result: List[str] = []
        if option == "all":
            for line in self.match_log:
                if name in line:
                    result.append(line)

            return result

        elif option == "player":
            for line in self.match_log:
                regex = r": \"{}.+?\"".format(re.escape(name))
                if name in line and re.search(regex, line):
                    result.append(line)

            return result

        elif option == "target":
            for line in self.match_log:
                regex = r"[^:] \"{}.+?\"".format(re.escape(name))
                if name in line and re.search(regex, line):
                    result.append(line)

            return result
        else:
            raise ValueError("Invalid option.")

    @staticmethod
    def get_time(line):
        """
            Search for the time in the given line.
        :param line: {string} given line.
        :return: {string} time of the line in the format : "hh:mm:ss"
        """
        return re.search(r"([0-9]{2}:){2}[0-9]{2}", line).group(0)

    @staticmethod
    def get_date(line):
        """
            Search for the date in the given line.
        :param line: {string} given line.
        :return: {string} time of the line in the format : "mm:dd:yyyy"
        """
        return re.search(r"([0-9]{2}/){2}[0-9]{4}", line).group(0)

    @staticmethod
    def remove_date_time(line):
        """
            Remove the date and the time of the given line.
        :param line: {string} Line.
        :return: {string} same line without the date and the time.
        """
        return re.sub(r"L ([0-9]{2}/){2}[0-9]{4} - ([0-9]{2}:){2}[0-9]{2}: ", "", line)

    def get_player_team(self, name):
        """
            Search the team of the player in the entire log (based on his name).
        :param name: {string} player's name.
        :raise ValueError: if no team is found for the given player.
        :return: {string} team of the given player.
        """
        for line in self.match_log:
            if re.search(r'joined team \"((Blue)|(Red))\"', line) is not None and name in line:
                return re.search(r'\"((Blue)|(Red))\"', line).group(0)[1:-1]
        else:
            raise ValueError("Team not found for player '{}'".format(name))

    def get_global_chat(self):
        """
            Search for the in-game global chat in the log.
        :return: {list: string} global chat.
        """
        chat = []
        for line in self.match_log:
            if "say" in line:
                chat.append(line)

        return chat

    def get_players_info(self):
        """
            Gather information for all the player.
        :return: {dic} information concerning all the players.
        """
        # TODO : get players' information
        pass
